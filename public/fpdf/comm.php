<?php

require('../fpdf/cellfit.php');


class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');
    $this->Ln(20);


    $this->Image('courtland.png',50.5,8,-720);
    $this->Image('courtland.png',151.5,8,-720);
    $this->setFont("Arial",'B',12);
    $this->Cell(100.5,3,"Courtland Realty.",0,0,"C");
    $this->Cell(100.5,3,"Courtland Realty.",0,1,"C");

     $this->setFont("Arial",'',8);
    $this->Cell(100.5,3,"Purok Puso, Cabaluna Street, Barangay Gredu,",0,0,"C");
    $this->Cell(100.5,3,"Purok Puso, Cabaluna Street, Barangay Gredu,",0,1,"C");
    $this->Cell(100.5,3,"Panabo City, 8015, Davao del Norte, Philippines 8000",0,0,"C");
    $this->Cell(100.5,3,"Panabo City, 8015, Davao del Norte, Philippines 8000",0,1,"C");
    $this->Cell(100.5,3,"Tel no. (084) 309 1947",0,0,"C");
    $this->Cell(100.5,3,"Tel no. (084) 309 1947",0,1,"C");

    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(4);
}

// Page fo$oter

}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->Image('dias.png',39,0,-300);
$pdf->Image('dias.png',139.7,0,-300);
$pdf->Ln(20);
$pdf->setFont("Arial",'B',12);
$pdf->Cell(100.5,3,"Diamante Realty",0,0,"C");
$pdf->Cell(100.5,3,"Diamante Realty",0,1,"C");

 $pdf->setFont("Arial",'',8);
$pdf->Cell(100.5,3,"Purok 7, Ising , Barangay Poblacion,",0,0,"C");
$pdf->Cell(100.5,3,"Purok 7, Ising , Barangay Poblacion,",0,1,"C");
$pdf->Cell(100.5,3,"Carmen, Davao del Norte, Philippines 8101",0,0,"C");
$pdf->Cell(100.5,3,"Carmen, Davao del Norte, Philippines 8101",0,1,"C");
$pdf->Cell(100.5,3,"Tel no. (084) 309 1947",0,0,"C");
$pdf->Cell(100.5,3,"Tel no. (084) 309 1947",0,1,"C");
// $this->Cell(260,5,$_GET["cid"],0,1,"C");
// Line break
$pdf->Ln(4);
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';//2n.QL8(TeB}qGk?4 for testing live
         $dbname = 'diamante_db';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }



         $sql = "SELECT CONCAT(cl.`firstName`,' ', cl.`lastName`) AS clientName, CONCAT(ag.`AgentFname`,' ', ag.`AgentLname`) AS agent, CONCAT(pop.`propertyName`,' ', 'block ', pl.`block`, ' lot ', pl.`lot`) AS property,
pay.`id`, pay.`Commission` FROM payments pay, propertylists pl, properties pop, clients cl, client__properties cp, agents ag
WHERE pay.`cp_id`=cp.`cp_id` AND cp.`client_id`=cl.`client_id` AND cp.`agent_id`=ag.`agent_id`  AND cp.`propertylistid`=pl.`propertylistid`AND pl.`propId`=pop.`propId` AND pay.`id`= '".$_GET['save']."' ";
            $result = mysqli_query($conn, $sql);


           if ($result->num_rows == 1) {
             $row = $result->fetch_assoc();



                     $pdf->SetFont('Times','B',10);
                     $pdf->SetTextColor(194,8,8);
                     $pdf->Cell(85.5,5,'Agent`s Copy' ,0,0,'R');
                     $pdf->Cell(103.5,5,'Diamante`s Copy'  ,0,1,'R');
                     $pdf->SetTextColor(0,0,0);
                     $pdf->Cell(103.5,5,'Agent Name : ' . $row["agent"] ,0,0,'L');
                     $pdf->Cell(100.5,5,'Agent Name : ' . $row["agent"] ,0,1,'L');
                     $pdf->Cell(103.5,5,'Date : ' . date("F j, Y") ,0,0,'L');
                     $pdf->Cell(100.5,5,'Date : ' .  date("F j, Y") ,0,1,'L');
                     $pdf->Cell(103.5,5,'Commission Control Num : AR-'  . $row["id"] ,0,0,'L');
                     $pdf->Cell(103.5,5,'Commission Control Num : AR-'  . $row["id"] ,0,1,'L');



                 $pdf->Cell(40.5,5,"Client Name",1,0,"C");
                 $pdf->CellFitScale(36.5,5,"Property",1,0,"C");
                 $pdf->CellFitScale(15.5,5,"Commission",1,0,"C");
                 $pdf->Cell(10,5,"",0,0,"C");
                 $pdf->Cell(40.5,5,"Client Name",1,0,"C");
                 $pdf->CellFitScale(36.5,5,"Property",1,0,"C");
                 $pdf->CellFitScale(15.5,5,"Commission",1,1,"C");
$pdf->Ln(2);




                $pdf->CellFitScale(40.5,5,$row["clientName"],0,0,"C");
                $pdf->CellFitScale(36.5,5,$row["property"],0,0,"C");
                $pdf->Cell(15.5,5,$row["Commission"] ,0,0,"C");
                  $pdf->Cell(10,5,"",0,0,"C");
                $pdf->CellFitScale(40.5,5,$row["clientName"],0,0,"C");
                $pdf->CellFitScale(36.5,5,$row["property"],0,0,"C");
                $pdf->Cell(15.5,5,$row["Commission"] ,0,0,"C");





                }

  $pdf->Ln(20);

  $pdf->Cell(103.5,5,'Release by : '.$_GET['user'],0,0,'L');
  $pdf->Cell(103.5,5,'Release by :  '.$_GET['user'],0,1,'L');
  $pdf->Ln(10);
  $pdf->Cell(85.5,5,'Agent`s Signature',0,0,'R');
  $pdf->Cell(103.5,5,'Agent`s Signature',0,1,'R');
         mysqli_close($conn);

$pdf->Output();
?>
