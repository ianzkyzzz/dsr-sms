@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Payment History') }}</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="clientList">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property</th>
      <th scope="col">Payment</th>
      <th scope="col">OR Number</th>
      <th scope="col">Method</th>
      <th scope="col">Date</th>


      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->firstName . " " . $item->lastName}}</td>
      <td align="center">{{$item->propertyName . " Block " . $item->block . " Lot " . $item->lot }}</td>
      <td align="center">{{$item->payment}}</td>
      <td align="center">{{$item->or_num}}</td>
      <td align="center">{{$item->paymentMethod}}</td>
      <td align="center">{{date("F j, Y", strtotime($item->created_at))}}</td>

      <td align="center">
      <a class="btn btn-sm btn-outline-success" href="#" data-ornum="{{$item->or_num}}" data-lot="{{$item->payment}}"  data-id="{{$item->id}}" data-method="{{$item->paymentMethod}}"  data-payment="{{$item->payment}}" data-toggle="modal" data-target="#editPayment" role="button">Edit</a>
        <a class="btn btn-sm btn-outline-info unlist" data-id="{{$item->id}}" href="#" role="button">delete</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>


<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<div class="modal fade" id="editPayment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/paymentEdit">
            @csrf
        <div class="form-group">


          <label for="exampleInputEmail1">OR NUMBER</label>
          <input type="text" class="form-control" id="ornum" name="ornum"  aria-describedby="emailHelp">
          <input type="hidden" name="payid" id="payid" />


        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">PAYMENT</label>
            <input type="textarea" class="form-control" id="payment" name="payment">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Method</label>
          <select class="form-control" id="Method" name="Method">
                <option value="">Select Method</option>
            <option value="Cash">Cash</option>
          <option value="Bank">Bank</option>


          </select>
        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
  @endif

</div>
<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });

  $('#editPayment').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var payment = button.data('payment')
      var ornum = button.data('ornum')
      var payid = button.data('id')
      var pmethod = button.data('method')


      // // alert(title)
      $('#payid').val(payid);
      $('#payment').val(payment);
      $('#ornum').val(ornum);
      $('#Method').val(pmethod);
      $('#parent').val(parent);

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });
  $("body").delegate('.unlist','click',function(){
           var id = $(this).attr("data-id");
    
           var r = confirm("Are you sure you want to delete payment?");
           if (r == true) {
            window.location.href ="/paymentDelete/"+id;
           } else {
             txt = "You pressed Cancel!";
           }

       })
  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>

@endsection
