@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>


                  <form action="/upload" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="file" class="form-control" name="image"/>
                    <input type="submit" class="btn btn-outline-primary" name="Upload" />

                  </form>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}


                    @endif

                    {{ __('You are logged in!') }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
