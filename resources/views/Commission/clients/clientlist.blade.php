@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('ClientList') }}</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Client</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="clientList">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Address</th>
      <th scope="col">birth Date</th>
      <th scope="col">Mobile</th>
      <th scope="col">Civil Status</th>

      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->firstName . " " . $item->middleName . " " . $item->lastName }}</td>
      <td align="center">{{$item->address}}</td>
      <td align="center">{{$item->birthDate}}</td>
      <td align="center">{{$item->mobileNumber}}</td>
      <td align="center">{{$item->civilStatus}}</td>

      <td align="center">
        <a class="btn btn-sm btn-outline-success" href="#" role="button">Edit</a>
        <a class="btn btn-sm btn-outline-info" href="{{'/clientProperties/'.$item->client_id.'/list'}}" role="button">Properties</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title"  id="exampleModalLabel">Add Client</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/client/create">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">First Name</label>
               <input type="text" class="form-control" id="firstName" name="firstName" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Middle Name</label>
             <input type="text" class="form-control" id="middleName" name="middleName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Last Name</label>
             <input type="text" class="form-control" id="lastName" name="lastName" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
           </div>
           <div class="form-row">
              <div class="form-group col-md-4">
                <label for="product_name">Client2</label>
                <input type="text" class="form-control" id="Client2" name="Client2" >
                <small id="asNameError" class="form-text text-muted"></small>

              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">Client3</label>
              <input type="text" class="form-control" id="Client3" name="Client3" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
              <div class="form-group col-md-4 mx-auto">
                <label for="Unit">Client4</label>
              <input type="text" class="form-control" id="Client4" name="Client4" >
                <small id="asTpypeError" class="form-text text-muted"></small>
              </div>
            </div>
           <div class="form-row">
           <div class="form-group col-md-8">
               <label for="Prod_categ">Address </label>
            <textarea id="address" name="address" class="form-control"> </textarea>
             </div>
             <div class="form-group col-md-4">
               <label for="product_name"> Birth Date</label>
               <input type="date" class="form-control" id="birthDate" name="birthDate" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             </div>
             <div class="form-row">
             <div class="form-group col-md-8">
                 <label for="Prod_categ">Place of Birth </label>
              <textarea id="placeofBirth" name="placeofBirth" class="form-control"> </textarea>
               </div>
               <div class="form-group col-md-4">
                 <label for="product_name">Gender</label>
                 <select class="form-control" name="gender" id="gender">
                   <option value="">-->Select Gender<--</option>
                   <option value="Male">Male</option>
                   <option value="Female">Female</option>
                   <option value="NotStated">Prefer not to state</option>
                 </select>

                 <small id="asNameError" class="form-text text-muted"></small>

               </div>
               </div>
             <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="product_name"> Mobile Number</label>
                  <input type="text" class="form-control" id="mobileNumber" name="mobileNumber" >
                  <small id="asNameError" class="form-text text-muted"></small>

                </div>
                <div class="form-group col-md-4 mx-auto">
                  <label for="Unit">Email Address</label>
                <input type="text" class="form-control" id="emailAddress" name="emailAddress" >
                  <small id="asTpypeError" class="form-text text-muted"></small>
                </div>
                <div class="form-group col-md-4 mx-auto">
                  <label for="product_name">Civil Status</label>
                  <select class="form-control" name="civilStatus" id="civilStatus">
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Separated">Separated</option>
                  </select>

                  <small id="asNameError" class="form-text text-muted"></small>
                </div>
              </div>
            <h5>Reference Person</h5>
            <div class="form-row">
               <div class="form-group col-md-3">
                 <label for="product_name">Reference Name</label>
                 <input type="text" class="form-control" id="referenceName" name="referenceName" >
                 <small id="asNameError" class="form-text text-muted"></small>

               </div>
               <div class="form-group col-md-6 mx-auto">
                 <label for="Unit">Address</label>
              <textarea class="form-control" id="referenceAddress" name="referenceAddress"> </textarea>
                 <small id="asTpypeError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-3 mx-auto">
                 <label for="Unit">Reference Contract</label>
               <input type="text" class="form-control" id="referenceContact" name="referenceContact" >
                 <small id="asTpypeError" class="form-text text-muted"></small>
               </div>
             </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('.timepicker').datetimepicker({

        format: 'HH:mm:ss'

    });

</script>
@endsection
