@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2> {{$agentz->AgentFname . "  " .$agentz->AgentLname }}'s Commissions</h2>
                  <h5>{{$agentz->AgentAddress}}</h5>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property Name</th>
      <th scope="col">lot</th>
      <th scope="col">block</th>
        <th scope="col">Commission</th>
        <th scope="col">Date</th>
          <th scope="col">Action</th>
    </tr>
  </thead>




@foreach($data as $items)
<a href="#" hidden>
{{$initial = $initial + $items->Commission }}
</a>

<tr>

  <th scope="row" align="center">{{$count++}}</th>
 <td align="center">{{$items->cname }}</td>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->lot}}</td>
  <td align="center">{{$items->block}}</td>
  <td align="center">{{number_format(round($items->Commission,2),2,'.',',')}}</td>
  <td align="center">{{date("F j, Y", strtotime($items->created_at))}}</td>
  <td align="center">


<a class="btn btn-sm btn-outline-info" href="{{'/Release/' . $items->id}}" role="button">Release</a>
  </td>



</tr>

@endforeach



<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Commission</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
</table>
<nav aria-label="Page navigation example">

</nav>



                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
