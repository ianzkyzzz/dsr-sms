@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                <h2>  {{$paymentsDetails[0]->firstName}} {{ $paymentsDetails[0]->lastName}}'s Payment History for</h2>
                <h3>  {{$paymentsDetails[0]->propertyName}} Block {{$paymentsDetails[0]->block}} Lot {{$paymentsDetails[0]->lot}}</h3>



                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<a href="{{'/fpdf/statementOfAccount.php/?cid='.$paymentsDetails[0]->cp_id}}"  class="btn btn-sm btn-outline-primary form-group" >Print</a>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">

  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">block</th>
      <th scope="col">lot</th>
        <th scope="col">Payment</th>
        <th scope="col">Payment for</th>
        <th scope="col">Date</th>
    </tr>
  </thead>




@foreach($data as $items)
<a href="#" hidden>
{{$initial = $initial + $items->payment + $items->otherpayment }}
</a>

<tr>

  <th scope="row" align="center">{{$count++}}</th>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->lot}}</td>
  <td align="center">{{$items->block}}</td>
  <td align="center">{{number_format(round($items->payment + $items->otherpayment,2),2,'.',',')}}</td>
    <td align="center">{{$items->paymentName}}</td>
  <td align="center">{{date("F j, Y", strtotime($items->created_at))}}</td>



</tr>

@endforeach

<tr>

  <th scope="row" align="center"></th>
  <td align="center">Total </td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>


</table>
<nav aria-label="Page navigation example">

</nav>



                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
