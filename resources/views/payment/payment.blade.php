@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Payment') }}</h2></div>
                @if(session()->has('message'))
                <div class="alert alert-success">{{session()->get('message')}} </div>
                @endif
                <x-alert />
                @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif

                <div class="card-body">
                  @if (Route::has('login'))

                    <div class="container-fluid">
                      <div class="container-fluid">
                        <form method="post" action="/payment/store">
                           @csrf

                      <div class="container">

                       <div class="form-row">
                         <div class="card-body" id="map" style="height: 450px;display: none">
                         <img src="{{asset('/img/full.png')}}" alt="{{asset('/storage/images/avatar.png')}}" style="vertical-align:middle"/>
                         </div>
                          <div class="form-group col-md-4">

                            <label for="Agent_name">Client Name</label>
                            <select name="Client" class="custom-select"  id="Client" >
                               <option  value="" >Select Client</option>

                            </select>

                          </div>
                          <div class="form-group col-md-4 mx-auto">
                            <label for="Unit">Property</label>
                            <select class="form-control" name="propList" id="propList">
                              <option value="">Select Property</option>

                            </select>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">Monthly Amortization</label>
                     <input type="text" class="form-control" id="monthlyAmortization" name="monthlyAmortization" readonly>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">PlanTerms</label>
                          <input type="text" class="form-control" id="terms" name="terms" readonly >
                            <small id="asTpypeError" class="form-text text-muted"></small>
                          </div>
                        </div>

                        <div class="form-row">

                           <div class="form-group col-md-3">

                             <label for="product_name">Total Contract Price</label>
                             <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>   <div class="form-group col-md-3">
                                <label for="product_name">Payment Method</label>
                                <select name="PaymentMethod" class="form-control"  id="PaymentMethod">
                                   <option value="">Select Method</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                </select>
                                <small id="asNameError" class="form-text text-muted"></small>
                              </div>

                           <div class="form-group col-md-3 mx-auto">
                            <label for="product_name">Due Date</label>
                               <input type="text" class="form-control" id="due" name="due" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>
                           <div class="form-group col-md-3 mx-auto">
                             <label for="product_name">Penalty</label>
                             <div class="form-check form-check-inline form-control">

                         <input class="form-check-input position-static" title="Check to Include Penalty" type="radio" id="blankCheckbox" value="1" aria-label="...">
                         <input type="text" class="form-control" id="Penalty" name="Penalty" placeholder="Penalty" value="0" readonly>
                         </div>

                                 </div>
                         </div>
                         <div class="form-row">
                           <div class="form-group col-md-2 mx-auto">
                             <label for="product_name">Total Paid</label>
                               <input type="text" class="form-control" id="Paid" name="Paid" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                                 </div>
                                 <div class="form-group col-md-2 mx-auto">
                                   <label for="product_name">Balance</label>
                                     <input type="text" class="form-control" id="Balance" name="Balance" readonly>
                                   <small id="asNameError" class="form-text text-muted"></small>

                                       </div>
                            <div class="form-group col-md-2 mx-auto">
                              <label for="product_name">Paid Amount</label>
                                <input type="text" class="form-control" id="Monthly" name="Monthly">
                                   <input type="hidden" id="comm" name="comm">
                                  <input type="hidden" id="comRelease" name="comRelease" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                                  </div>
                                  <div class="form-group col-md-2 mx-auto">
                                    <label for="product_name">OR NUMBER</label>
                                      <input type="text" class="form-control" id="OR" name="OR" value="000-0000-000">

                                    <small id="asNameError" class="form-text text-muted"></small>

                                        </div>



                            <div class="form-group col-md-4 mx-auto">
                              <label for="product_name">Details</label>
                                <input type="text" class="form-control" id="details" name="details">
                                  <input type="hidden" id="cid" name="cid" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                            </div>

                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-4 mx-auto">
                                <label for="product_name">Custom Agent Commission</label>
                                  <input type="text" class="form-control" id="cusCommission" name="cusCommission">
                                </div>
                              <div class="form-group col-md-4 mx-auto">
                                <label for="product_name">Date</label>
                                  <input type="date" class="form-control" id="date" name="date">
                                </div>

                                </div>
                        <button type="submit"  class="btn btn-primary">Save changes</button>
                      </form>
                      </div>
                      </div>


                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

                      $('#Client').select2({
                        ajax:{
                          url:"{{route('client.getClientList')}}",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                           _token:CSRF_TOKEN,
                            search: params.term,
                              col: 'vdn'
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });

$('#propList').on('change', function () {
   let cp_id = $(this).val();
   $.ajax({
   type: 'GET',
   dataType: 'json',
   url: '/clientPropertyFetch/' + cp_id ,
   success: function (data) {
     console.log(data[0]);
         $('#monthlyAmortization').val(data[0].get_clientproperty_relation.monthlyAmortization);
         $('#contractPrice').val(data[0].contractPrice);
         $('#terms').val(data[0].get_clientproperty_relation.PlanTerms);
         $('#due').val(data[0].get_clientproperty_relation.dueDate);
         $('#comm').val(data[0].get_clientproperty_relation.Commission);
         $('#Monthly').val(data[0].get_clientproperty_relation.monthlyAmortization);
         $('#Paid').val(data[0].get_clientproperty_relation.totalPaid);
         $('#comRelease').val(data[0].get_clientproperty_relation.comRelease);
         $('#comm').val(data[0].get_clientproperty_relation.Commission);
         var x = (data[0].contractPrice)-(data[0].get_clientproperty_relation.totalPaid);
         $('#Balance').val((data[0].contractPrice)-(data[0].get_clientproperty_relation.totalPaid));


if (x<=0) {
  $('#Monthly').prop('readonly', true);
  $('#OR').prop('readonly', true);
  $('#details').prop('readonly', true);
  $('#Monthly').val('FULLY PAID');
  $('#OR').val('FULLY PAID');
  $('#details').val('FULLY PAID');
  $("#map").show();
}
else {
  {
    $("#map").hide();
  }
}


   //alert(data[0].contractPrice);


   }
});

});
                    $('#blankCheckbox').on('change', function () {

                      var pen = parseFloat(($('#monthlyAmortization').val()*0.03) );
                      var mon = parseFloat($('#monthlyAmortization').val());
                      if(this.checked) {
                      $('#Penalty').val(($('#monthlyAmortization').val()*0.03).toFixed(2));
                      $('#Monthly').val((pen+mon).toFixed(2));
                      }
                      else {
                          $('#Penalty').val(0);
                      }
                    });

                    $('#Client').on('change', function () {
                                   let client_id = $(this).val();

                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/clientPropertiesList/' + client_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element => {

                                     // console.log(element.getpropertylist_relation.propertyName);
                                       $('#propList').append(`<option value="${element.get_clientproperty_relation.cp_id}">' ${element.getpropertylist_relation.propertyName} Block ${element['block']} Lot ${element['lot']} </option>`);
                                       });
                                   }
                               });
                           });
});
</script>
@endsection
