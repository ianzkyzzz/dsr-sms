<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

  public function adduser(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
    $user = new User();
  $user->name = $request->input('name');
  $user->email = $request->input('email');
  $user->password = bcrypt($request->input('password'));
  $user->branch = $request->input('branch');
  $user->role = $request->input('role');

  // assign other properties
  $user->save();
   return redirect()->back()->with('message', 'User Added Successfully');
    // return view('dashboard');
  }
  public function loaduserform()
  {
     return view('user.userform');
    // return view('dashboard');
  }
    public function userlists()
    {
      $data = DB::table('users')
      ->where('isActive', '1')
      ->get();
       return view('user.userlisting',['data'=>$data])->with('count',1);
      // return view('dashboard');
    }
    public function profile()
    {

       return view('user.userprofile');
      // return view('dashboard');
    }
    public function uploadAvatar(Request $request)
    {
      if ($request->hasfile('image')) {
        $filename = $request->image->getClientOriginalName();
        $request->image->storeAs('images', $filename, 'public');
        auth()->user()->update(['avatar'=>$filename]);

      }
      return redirect()->back()->with('message', 'User Avatar Updated Successfully');


    }
    public function editUser(Request $request)
    {
        $branch =  $request->input('branch');
        $name =  $request->input('name');
        auth()->user()->update(['name'=>$name,'branch'=>$branch ]);


      return redirect()->back()->with('message', 'User Updated Successfully');


    }

}
