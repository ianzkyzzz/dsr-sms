<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Agent;
use App\Models\Client_Property;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
  public function index()
  {
      return view('payment.payment');
  }
  public function scheduleSMS()
  {
    $data = DB::select(DB::raw('SELECT cl.Fname,cl.Lname, cp.dueDate, pr.`propertyName`, pl.`block`, pl.`lot`,cl.`ContactNumber` FROM tbl_client_properties cp, tbl_client cl, tbl_propertyparent pr, tbl_propertylist pl WHERE
 (cp.dueDate = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL -3 DAY), "%d")) AND cp.client_id=cl.client_id AND cp.property_id=pl.property_id AND pl.propertyParentID=pr.id AND cp.`active`="1" AND cp.`fullyPaid`="0"'));




foreach($data as $item)
{
  $apicode="ST-DSRDO875467_BTAET";
  $passwd="8trubl&qfs";
  $numbexr=$item->ContactNumber;
  $message = "Hi " . $item->Fname. " " .$item->Lname. ", This is a friendly reminder of Your property " .$item->propertyName ." on block ". $item->block.", lot ".$item->lot." dated every " . $item->dueDate." of the month. Please pay on or before due date to avoid penalties. Please disregard if payment has been settled.
This is System Generated Message from RNS.";
   $this->itexmo($numbexr,$message,$apicode,$passwd);
}

  }
  public function itexmo($number,$message,$apicode,$passwd){
		$url = 'https://www.itexmo.com/php_api/api.php';
		$itexmo = array('1' => $number, '2' => $message, '3' => $apicode, 'passwd' => $passwd);
		$param = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($itexmo),
			),
		);
		$context  = stream_context_create($param);
		return file_get_contents($url, false, $context);
}
  private function updatePropertyPaid($cp_id,$amount,$commRell)
  {
    $data=Client_Property::find($cp_id);
    $data->totalPaid=$amount;
    $data->comRelease=$commRell;
    $data->save();
  }
  private function nowFullPaid($cp_id)
  {
    $data=Client_Property::find($cp_id);
    $data->isFullyPaid=1;
    $data->save();
  }

  public function releaseComm($id)
  {
    $user=  Auth::user()->name;
    $data=Payment::find($id);
    $ldate = date('Y-m-d');
    $data->releaseDate=$ldate;
    $data->isCOmRelease=1;
    $data->systemPay2=50;
    $data->save();
    header("Location: /fpdf/comm.php/?save=$id&user=$user");
die();
  }
  public function releaseCommDate(Request $request)
  {
    $user=  Auth::user()->name;
$id = $request->agent_id;
$date = $request->date;
    header("Location: /fpdf/commDate.php/?save=$id&user=$user&date=$date");
die();
  }
  public function getUnclaimed(Request $request)
  {

    $agent_id = $request->agent_id;
      $agentz = Agent::find($agent_id);
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'payments.Commission','client__properties.PlanTerms','payments.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','client__properties.agent_id')
    ->where('client__properties.agent_id', '=', $agent_id)
    ->where('payments.Commission', '>', 0)
    ->where('payments.isCOmRelease', '=', 0)
        ->where('payments.isActive', '=', 1)
    ->get();
    // dd($data);
      return view('Commission.agentcommission', compact('data', 'agentz'))->with('count',1)->with('initial',0);
  }

  public function getRelease(Request $request)
  {
$selectvalue="";
    $agent_id = $request->agent_id;
    $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);
      $balance ="0";
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'payments.Commission','payments.releaseDate','payments.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.releaseDate','client__properties.agent_id')
    ->where('client__properties.agent_id', '=', $agent_id)
    ->where('payments.Commission', '>', 0)
    ->where('payments.isCOmRelease', '=', 1)
    ->where('payments.isActive', '=', 1)
    ->get();
    $select = DB::table('client__properties')
    ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
    ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS name")),'client__properties.cp_id')
    ->where('client__properties.agent_id', '=', $agent_id)
    ->get();


    // dd($data);
      return view('Commission.CommissionHistory', compact('data', 'agentz','select','selectvalue','balance'))->with('balance','')->with('count',1)->with('gselect',"")->with('agent_id',$agent_id)->with('date',$date)->with('initial',0);
  }
  public function getReleaseClient(Request $request)
  {

    $agent_id = $request->agent_id;
    $gg= $request->client;
  $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);

      $data = DB::table('payments')
      ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'payments.Commission','payments.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.releaseDate','client__properties.agent_id')
      ->where('client__properties.agent_id', '=', $agent_id)
      ->where('payments.Commission', '>', 0)
      ->where('client__properties.cp_id', '=', $gg)
      ->where('payments.isCOmRelease', '=', 1)
      ->where('payments.isActive', '=', 1)
      ->get();
      $balance = DB::table('client__properties')
        ->select('client__properties.commTotal')
        ->where('client__properties.cp_id' , '=', $gg)
        ->get();
        // dd($balance);
        $select = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS name")),'client__properties.cp_id')
        ->where('client__properties.agent_id', '=', $agent_id)
        ->get();
        $selectvalue = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS namez")))
        ->where('client__properties.agent_id', '=', $agent_id)
        ->where('client__properties.cp_id', '=', $gg)
        ->get();

    // dd($data);
      return view('Commission.CommissionHistory', compact('data', 'agentz','select','selectvalue',))->with('date',$date)->with('balance',$balance[0]->commTotal)->with('gselect',$gg)->with('agent_id',$agent_id)->with('count',1)->with('initial',0);
  }

  public function getReleaseDate(Request $request)
  {

    $agent_id = $request->agent_id;
    $date = $request->relDate;
      $agentz = Agent::find($agent_id);

        $data = DB::table('payments')
        ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
        ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'payments.Commission','payments.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.releaseDate','client__properties.agent_id')
        ->where('client__properties.agent_id', '=', $agent_id)
        ->where('payments.Commission', '>', 0)
        ->where('payments.releaseDate', '=', $date)
        ->where('payments.isCOmRelease', '=', 1)
            ->where('payments.isActive', '=', 1)
        ->get();
        $select = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS name")),'client__properties.cp_id')
        ->where('client__properties.agent_id', '=', $agent_id)
        ->get();
        $selectvalue="";






    // dd($data);
      return view('Commission.CommissionHistory', compact('data', 'agentz','select','selectvalue'))->with('balance','')->with('date',$date)->with('agent_id',$agent_id)->with('count',1)->with('initial',0);
  }

  public function listPaid(Request $request)
  {

    $cp_id = $request->cp_id;
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
    ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('payments.otherpayment','payments.payment','payments.paymentName', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','client__properties.agent_id')
    ->where('client__properties.cp_id', '=', $cp_id)
        ->where('payments.isActive', '=', 1)
    ->get();
// dd($data);
    $paymentsDetails = DB::table('client__properties')
    ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('clients','clients.client_id', '=', 'client__properties.client_id')
    ->join('properties','properties.propId', '=', 'propertylists.propId')
    ->where('client__properties.cp_id', '=', $cp_id)
    ->where('client__properties.isActive', '=', 1)
    ->select('properties.propertyName','client__properties.cp_id','client__properties.comRelease','propertylists.lot','propertylists.block','clients.firstName', 'clients.firstName', 'clients.lastName')
    ->get();
    // dd($data);
      return view('clientProperties.clientPaymentHistory', compact('data','paymentsDetails'))->with('count',1)->with('initial',0);
  }
}
