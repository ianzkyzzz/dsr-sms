<?php

use Illuminate\Support\Facades\Route;
// use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\PropertylistController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sms', [App\Http\Controllers\PaymentController::class, 'scheduleSMS'])->name('home');
Route::get('/home', function () {
    return view('dashboard');
});
Route::get('/users', 'App\Http\Controllers\UserController@index');
// Route::get('/hjhj', function () {
//     return 'Hello world';

// Route::get('/aboutus', [PropertyController::class, 'index']);

Auth::routes();





Route::group([
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::get('reports/agents', 'ReportController@agents');
});


// Route::post('/registerNow', [App\Http\Controllers\UserController::class, 'adduser']);



//Route::resource('property', 'PropertyController');
//Route::resource('client', 'App\Http\Controllers\ClientController');
