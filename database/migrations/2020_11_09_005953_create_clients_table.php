<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments("client_id");
            $table->string("firstName");
            $table->string("middleName")->nullable();
            $table->string("lastName");
            $table->string("client2")->nullable();
            $table->string("client3")->nullable();
            $table->string("client4")->nullable();

            $table->dateTime("birthDate");
            $table->string("placeofBirth");
            $table->string("civilStatus");
            $table->string("mobileNumber");
            $table->string("gender");
            $table->string("emailAddress")->nullable();
            $table->string("address");
            $table->string("referenceName");
            $table->string("referenceAddress");
            $table->string("referenceContact");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
